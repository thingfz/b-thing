#!/usr/bin/env bash

# Restart CodeDeploy agent to release the memory.
at -M now + 2 minute <<< $'service codedeploy-agent restart'
