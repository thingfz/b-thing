#!/usr/bin/env bash

## dir to project path
cd /home/ubuntu/b-thing

# Change storage and bootstrap/cache permission as 777
sudo find /home/ubuntu/b-thing -type d -exec chmod ug+s {} \;
sudo chown -R www-data:www-data storage bootstrap/cache
sudo chmod -R ugo+rwx storage bootstrap/cache

sudo cp provision/.env.production .env

sudo composer install

php artisan config:clear
php artisan migrate:refresh --seed
php artisan view:clear