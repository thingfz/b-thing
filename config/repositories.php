<?php

return [
    'App\\Contracts\\UserRepository' => [
        'class' => 'App\\Services\\UserRepositoryImpl',
        'shared' => false,
        'singleton' => true,
    ],
];
