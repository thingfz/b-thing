<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<ul class="m-nav-sticky" style="margin-top: 30px;">
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left"
        data-original-title="Layout Builder">
        <a href="?page=builder&amp;demo=default"><i class="la la-cog"></i></a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left"
        data-original-title="Purchase">
        <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
           target="_blank"><i class="la la-cart-arrow-down"></i></a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left"
        data-original-title="Documentation">
        <a href="https://keenthemes.com/metronic/documentation.html" target="_blank"><i class="la la-code-fork"></i></a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="Support">
        <a href="https://keenthemes.com/forums/forum/support/metronic5/" target="_blank"><i class="la la-life-ring"></i></a>
    </li>
</ul>
<!--begin::Base Scripts -->
<script src="{{url('/js/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{url('/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{url('/js/_all.js')}}" type="text/javascript"></script>