@extends('auth.layouts.app')

@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2"
             id="m_login" style="background-image: url({{url('assets/images/bg.jpg')}});">
            <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a hrlogo-1.pngef="#">
                            <img src="{{url('assets/images/logo.png')}}">
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">{{__('auth.signInToAdmin')}}</h3>
                        </div>
                        <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group m-form__group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <input class="form-control m-input {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       type="text"
                                       placeholder="{{ __('auth.email') }}"
                                       name="email"
                                       autocomplete="off"
                                       value="{{ old('email') }}"
                                       required autofocus
                                >
                                @if ($errors->has('email'))
                                    <div id="email-error" class="form-control-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group m-form__group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <input class="form-control m-input m-login__form-input--last {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       type="password"
                                       placeholder="{{ __('auth.password') }}"
                                       name="password"
                                >
                                @if ($errors->has('password'))
                                    <div id="email-error" class="form-control-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                            <div class="row m-login__form-sub">
                                <div class="col m--align-left m-login__form-left">
                                    <label class="m-checkbox  m-checkbox--focus">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ __('auth.rememberMe') }}
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col m--align-right m-login__form-right">
                                    <a href="{{ route('password.request') }}" id="m_login_forget_password"
                                       class="m-link">
                                        {{ __('auth.forgetPassword') }}
                                    </a>
                                </div>
                            </div>
                            <div class="m-login__form-action">
                                <button id="m_login_signin_submit"
                                        class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                    {{ __('auth.signIn') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection