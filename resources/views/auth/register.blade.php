@extends('auth.layouts.app')

@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--2 m-login-2--skin-2 m-login--signup"
             id="m_login" style="background-image: url({{url('assets/images/bg.jpg')}});">
            <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a hrlogo-1.pngef="#">
                            <img src="{{url('assets/images/logo.png')}}">
                        </a>
                    </div>
                    <div class="m-login__signup">
                        <div class="m-login__head">
                            <h3 class="m-login__title">{{__('auth.signUp')}}</h3>
                            <div class="m-login__desc">{{__('auth.signUpToAdmin')}}</div>
                        </div>
                        <form class="m-login__form m-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group m-form__group  {{ $errors->has('name') ? ' has-danger' : '' }}">
                                <input class="form-control m-input {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       type="text"
                                       placeholder="{{ __('auth.name') }}" name="name">
                                @if ($errors->has('name'))
                                    <div id="name-error" class="form-control-feedback">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group m-form__group  {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <input class="form-control m-input {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       type="text"
                                       placeholder="{{ __('auth.email') }}" name="email"
                                       autocomplete="off">
                                @if ($errors->has('email'))
                                    <div id="email-error" class="form-control-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group m-form__group  {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <input class="form-control m-input {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       type="password"
                                       placeholder="{{ __('auth.password') }}"
                                       name="password">
                                @if ($errors->has('password'))
                                    <div id="password-error" class="form-control-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input m-login__form-input--last"
                                       type="password"
                                       placeholder="{{ __('auth.confirmPassword') }}"
                                       name="password_confirmation">
                            </div>
                            <div class="m-login__form-action">
                                <button id="m_login_signup_submit"
                                        class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                                    {{ __('auth.signUp') }}
                                </button>&nbsp;&nbsp;
                                <button id="m_login_signup_cancel"
                                        class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom  m-login__btn">
                                    {{ __('auth.cancel') }}
                                </button>
                            </div>
                        </form>
                    </div>
                    {{--@include("auth.passwords.reset")--}}
                </div>
            </div>
        </div>
    </div>
@endsection