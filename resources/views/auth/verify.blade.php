@extends('auth.layouts.app')

@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2"
             id="m_login" style="background-image: url({{url('assets/images/bg.jpg')}});">
            <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a hrlogo-1.pngef="#">
                            <img src="{{url('assets/images/logo.png')}}">
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">{{ __('Verify Your Email Address') }}</h3>
                        </div>

                        @if (session('resent'))
                            <div class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif
                        {{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }}, <a
                                href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                    </div>

                    {{--@include("auth.register")--}}
                    {{--@include("auth.passwords.reset")--}}
                </div>
            </div>
        </div>
    </div>
@endsection