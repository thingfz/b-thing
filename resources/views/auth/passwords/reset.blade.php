@extends('auth.layouts.app')

@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--2 m-login-2--skin-2 m-login--signup"
             id="m_login" style="background-image: url({{url('assets/images/bg.jpg')}});">
            <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a hrlogo-1.pngef="#">
                            <img src="{{url('assets/images/logo.png')}}">
                        </a>
                    </div>
                    <div class="m-login__signup">
                        <div class="m-login__head">
                            <h3 class="m-login__title">{{__('auth.reset')}}</h3>
                        </div>
                        <form class="m-login__form m-form" method="POST" action="{{ route('password.update') }}">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group m-form__group  {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <input class="form-control m-input {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       type="text"
                                       placeholder="{{ __('auth.email') }}"
                                       name="email"
                                       autocomplete="off"
                                       required
                                >
                                @if ($errors->has('email'))
                                    <div id="email-error" class="form-control-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group m-form__group  {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <input class="form-control m-input {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       type="password"
                                       placeholder="{{ __('auth.password') }}"
                                       name="password"
                                       required
                                >
                                @if ($errors->has('password'))
                                    <div id="password-error" class="form-control-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input m-login__form-input--last"
                                       type="password"
                                       placeholder="{{ __('auth.confirmPassword') }}"
                                       name="password_confirmation"
                                       required
                                >
                            </div>
                            <div class="m-login__form-action">
                                <button id="m_login_signup_submit"
                                        class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                                    {{ __('auth.reset') }}
                                </button>&nbsp;&nbsp;
                                <a id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom m-login__btn" href="{{route('login')}}">Cancel</a>
                            </div>
                        </form>
                    </div>
                    {{--@include("auth.passwords.reset")--}}
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--<div class="row justify-content-center">--}}
{{--<div class="col-md-8">--}}
{{--<div class="card">--}}
{{--<div class="card-header">{{ __('Reset Pasdsdssword') }}</div>--}}

{{--<div class="card-body">--}}
{{--<form method="POST" action="{{ route('password.update') }}">--}}
{{--@csrf--}}

{{--<input type="hidden" name="token" value="{{ $token }}">--}}

{{--<div class="form-group row">--}}
{{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>--}}

{{--@if ($errors->has('email'))--}}
{{--<span class="invalid-feedback" role="alert">--}}
{{--<strong>{{ $errors->first('email') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group row">--}}
{{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

{{--@if ($errors->has('password'))--}}
{{--<span class="invalid-feedback" role="alert">--}}
{{--<strong>{{ $errors->first('password') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group row">--}}
{{--<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group row mb-0">--}}
{{--<div class="col-md-6 offset-md-4">--}}
{{--<button type="submit" class="btn btn-primary">--}}
{{--{{ __('Reset Password') }}--}}
{{--</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endsection--}}
