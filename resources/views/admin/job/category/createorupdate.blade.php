@extends('layouts.app')
@section('title')
    <title>Job Category {{!isset($jobCategory)? "Create" : "Edit"}} - Thing</title>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Job Category Module
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="{{url('/')}}" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{'users'}}" class="m-nav__link">
                                <span class="m-nav__link-text">Job Category Module</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{'users'}}" class="m-nav__link">
                                <span class="m-nav__link-text">Job Category {{!isset($jobCategory)? "Create" : "Edit"}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
                            <h3 class="m-portlet__head-text">
                                {{isset($jobCategory)? "Job Category Edit" : "New Job Category"}}
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--state m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                      method="POST"
                      action="{{ isset($jobCategory)? url("jobcategories/".$jobCategory->id) : url("jobcategories") }}"
                      enctype="multipart/form-data"
                >
                    @csrf
                    {{ isset($jobCategory)? method_field("PUT") : null }}
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row {{ $errors->has("name") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">
                                    Full Name
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="name"
                                           class="form-control m-input m-input--solid"
                                           placeholder="Enter full name"
                                           value="{{isset($jobCategory)? old('name')?:$jobCategory->name : old('name')}}"
                                           required
                                    >
                                    @if ($errors->has('name'))
                                        <div id="name-error" class="form-control-feedback">
                                            {{ $errors->first('name') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter job category name</span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row {{ $errors->has("description") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">
                                    Description
                                </label>
                                <div class="col-lg-6">
                                    <textarea class="form-control m-input m-input--solid" id="description"
                                              name="description"
                                              rows="3">{{isset($job)? old('description')?:$job->description : old('description')}}</textarea>
                                    @if ($errors->has('description'))
                                        <div id="description-error" class="form-control-feedback">
                                            {{ $errors->first('description') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter your job category description</span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row {{ $errors->has("description") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label" for="m_form_parent">
                                    Parent Category
                                </label>
                                <div class="col-lg-6">
                                    <select class="form-control m-bootstrap-select"
                                            id="m_form_parent"
                                            name="parentId"
                                            tabindex="-98">
                                        @php
                                            foreach($jobCategories as $item) renderNode($item);
                                            function renderNode($node) {
                                              echo "<option value={$node->id}>";
                                              $i = $node->depth;
                                              while ($i) {
                                                   echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                                   $i--;
                                              }
                                              echo "{$node->name}";
                                              if ( $node->children()->count() > 0 ) {
                                                 foreach($node->children as $child) renderNode($child);
                                              }
                                              echo "</option>";
                                            }
                                        @endphp
                                    </select>
                                    <span class="m-form__help">Please select parent category</span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Image</label>
                                <div class="col-lg-6">
                                    <input type="file"
                                           class="filepond"
                                           name="image_url"
                                           accept="image/png, image/jpeg, image/gif"
                                           data-max-file-size="3MB"
                                           @if(isset($jobCategory) && !empty($jobCategory->image_url))
                                           data-exist-file="{{$jobCategory->image_url}}"
                                            @endif
                                    />
                                    @if ($errors->has('image_url'))
                                        <div id="avatar-error" class="form-control-feedback">
                                            {{ $errors->first('image_url') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{url('users')}}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
@endsection

@section('footerScript')
    <script src="{{url('/assets/demo/default/custom/crud/forms/widgets/input-mask.js')}}"
            type="text/javascript">
    </script>
@endsection