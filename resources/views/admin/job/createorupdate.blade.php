@extends('layouts.app')
@section('title')
    <title>Job {{!isset($job)? "Create" : "Edit"}} - Thing</title>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Job Module
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="{{url('/')}}" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{'jobs'}}" class="m-nav__link">
                                <span class="m-nav__link-text">Job Module</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{'jobs'}}" class="m-nav__link">
                                <span class="m-nav__link-text">Job {{!isset($job)? "Create" : "Edit"}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
                            <h3 class="m-portlet__head-text">
                                {{isset($job)? "Job Edit" : "New Job"}}
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--state m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                      method="POST" action="{{ isset($job)? url("jobs/".$job->id) : url("jobs") }}"
                      enctype="multipart/form-data"
                >
                    @csrf
                    {{ isset($job)? method_field("PUT") : null }}
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-form__group form-group row">
                                <label class="col-lg-3 col-form-label">Status</label>
                                <div class="col-lg-6">
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status"
                                                   @if(isset($job))
                                                   @if(old("status"))
                                                   {{$job->status === old("status")? "checked" : ""}}
                                                   @else
                                                   {{$job->status === \App\Models\Job::STATUS_APPROVED? "checked" : ""}}
                                                   @endif
                                                   @else
                                                   @if(old("status"))
                                                   {{\App\Models\Job::STATUS_APPROVED === old("status")? "checked" : ""}}
                                                   @else
                                                   checked
                                                   @endif
                                                   @endif
                                                   value="{{\App\Models\Job::STATUS_APPROVED}}"> Approved
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status"
                                                   @if(isset($job))
                                                   @if(old("status"))
                                                   {{$job->status === old("status")? "checked" : ""}}
                                                   @else
                                                   {{$job->status === \App\Models\Job::STATUS_PENDING? "checked" : ""}}
                                                   @endif
                                                   @else
                                                   @if(old("status"))
                                                   {{\App\Models\Job::STATUS_PENDING === old("status")? "checked" : ""}}
                                                   @endif
                                                   @endif
                                                   value="{{\App\Models\Job::STATUS_PENDING}}"> Pending
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status"
                                                   @if(isset($job))
                                                   @if(old("status"))
                                                   {{$job->status === old("status")? "checked" : ""}}
                                                   @else
                                                   {{$job->status === \App\Models\Job::STATUS_REJECTED? "checked" : ""}}
                                                   @endif
                                                   @else
                                                   @if(old("status"))
                                                   {{\App\Models\Job::STATUS_REJECTED === old("status")? "checked" : ""}}
                                                   @endif
                                                   @endif
                                                   value="{{\App\Models\Job::STATUS_REJECTED}}"> Rejected
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="m-form__group form-group row">
                                <label class="col-6 col-lg-3 col-form-label">Featured</label>
                                <div class="col-6 col-lg-2">
											<span class="m-switch m-switch--icon m-switch--primary">
												<label>
						                        <input type="checkbox"
                                                       @if(isset($job))
                                                       @if(old("is_feature"))
                                                       checked
                                                       @else
                                                       {{$job->is_feature? "checked" : ""}}
                                                       @endif
                                                       @endif
                                                       name="is_feature">
						                        <span></span>
						                        </label>
						                    </span>
                                </div>
                                <label class="col-6 col-lg-2 col-form-label">Negotiable</label>
                                <div class="col-6 col-lg-3">
											<span class="m-switch m-switch--icon m-switch--accent">
												<label>
						                        <input type="checkbox"
                                                       @if(isset($job))
                                                       @if(old("is_negotiable"))
                                                       checked
                                                       @else
                                                       {{$job->is_negotiable? "checked" : ""}}
                                                       @endif
                                                       @endif
                                                       name="is_negotiable">
						                        <span></span>
						                        </label>
						                    </span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row {{ $errors->has("title") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">
                                    Title
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="title"
                                           class="form-control m-input m-input--solid"
                                           placeholder="Enter Job Title"
                                           value="{{isset($job)? old('title')?:$job->title : old('title')}}"
                                    >
                                    @if ($errors->has('title'))
                                        <div id="title-error" class="form-control-feedback">
                                            {{ $errors->first('title') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter your job title</span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row {{ $errors->has("phone") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">
                                    Contact
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text"
                                               name="phone"
                                               id="m_inputmask_3"
                                               class="form-control m-input m-input--solid"
                                               value="{{isset($job)? old('phone')?:$job->phone : old('phone')}}"
                                               placeholder="Phone number"
                                               required
                                        >
                                    </div>
                                    @if ($errors->has('phone'))
                                        <div id="phone-error" class="form-control-feedback">
                                            {{ $errors->first('phone') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">We'll never share your phone with anyone else</span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row {{ $errors->has("salary") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">
                                    Salary
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="number" name="salary"
                                           class="form-control m-input m-input--solid"
                                           placeholder="Enter Job Salary"
                                           value="{{isset($job)? old('salary')?:$job->salary : old('salary')}}"
                                           required
                                    >
                                    @if ($errors->has('salary'))
                                        <div id="salary-error" class="form-control-feedback">
                                            {{ $errors->first('salary') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter your job salary</span>
                                </div>
                            </div>

                            <div class="m-form__group form-group row">
                                <label class="col-3 col-form-label">
                                    Sort
                                </label>
                                <div class="col-lg-6">
                                    <input type="number" name="sort_order"
                                           class="form-control m-input m-input--solid"
                                           placeholder="Enter Job Sort Order"
                                           value="{{isset($job)? old('sort_order')?:$job->sort_order : old('sort_order')}}"
                                    >
                                    @if ($errors->has('sort_order'))
                                        <div id="title-error" class="form-control-feedback">
                                            {{ $errors->first('sort_order') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter your Sort Order</span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row {{ $errors->has("description") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">
                                    Description
                                </label>
                                <div class="col-lg-6">
                                    <textarea class="form-control m-input m-input--solid" id="description"
                                              name="description"
                                              rows="3">{{isset($job)? old('description')?:$job->description : old('description')}}</textarea>
                                    @if ($errors->has('description'))
                                        <div id="description-error" class="form-control-feedback">
                                            {{ $errors->first('description') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter your job description</span>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Images</label>
                                <div class="col-lg-6">
                                    <input type="file"
                                           class="filepond"
                                           name="images[]"
                                           accept="image/png, image/jpeg, image/gif"
                                           data-max-file-size="3MB"
                                           @if(isset($job) && $job->images->isNotEmpty())
                                           data-exist-file="{{$job->images->pluck('url')}}"
                                           @endif
                                           multiple
                                    />
                                    <span class="m-form__help">Multiple images are support</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{url('jobs')}}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
@endsection

@section('footerScript')
    <script src="{{url('/assets/demo/default/custom/crud/forms/widgets/input-mask.js')}}"
            type="text/javascript">
    </script>
@endsection