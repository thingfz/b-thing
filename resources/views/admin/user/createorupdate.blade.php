@extends('layouts.app')
@section('title')
    <title>User {{!isset($user)? "Create" : "Edit"}} - Thing</title>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        User Module
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="{{url('/')}}" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{'users'}}" class="m-nav__link">
                                <span class="m-nav__link-text">User Module</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{'users'}}" class="m-nav__link">
                                <span class="m-nav__link-text">User {{!isset($user)? "Create" : "Edit"}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
                            <h3 class="m-portlet__head-text">
                                {{isset($user)? "User Edit" : "New User"}}
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--state m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                      method="POST" action="{{ isset($user)? url("users/".$user->id) : url("users") }}"
                      enctype="multipart/form-data"
                >
                    @csrf
                    {{ isset($user)? method_field("PUT") : null }}
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row {{ $errors->has("name") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">
                                    Full Name
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="name"
                                           class="form-control m-input m-input--solid"
                                           placeholder="Enter full name"
                                           value="{{isset($user)? old('name')?:$user->name : old('name')}}"
                                           required
                                    >
                                    @if ($errors->has('name'))
                                        <div id="name-error" class="form-control-feedback">
                                            {{ $errors->first('name') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter your full name</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row {{ $errors->has("email") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">Email address <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="email" name="email"
                                           class="form-control m-input m-input--solid"
                                           placeholder="Enter email"
                                           value="{{isset($user)? old('email')?:$user->email : old('email')}}"
                                           required
                                    >
                                    @if ($errors->has('email'))
                                        <div id="email-error" class="form-control-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">We'll never share your email with anyone else</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row {{ $errors->has("phone") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">Contact</label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text"
                                               name="phone"
                                               id="m_inputmask_3"
                                               class="form-control m-input m-input--solid"
                                               value="{{isset($user)? old('phone')?:$user->phone : old('phone')}}"
                                               placeholder="Phone number">
                                    </div>
                                    @if ($errors->has('phone'))
                                        <div id="phone-error" class="form-control-feedback">
                                            {{ $errors->first('phone') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">We'll never share your phone with anyone else</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row {{ $errors->has("password") ? "has-danger" : "" }}">
                                <label class="col-lg-3 col-form-label">Password <span
                                            class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend"></div>
                                        <input type="password" name="password"
                                               class="form-control m-input m-input--solid"
                                               placeholder="Enter password"
                                                {{isset($user)? "" : "required"}}
                                        >
                                    </div>
                                    @if ($errors->has('password'))
                                        <div id="password-error" class="form-control-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
                                    <span class="m-form__help">Please enter your password</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Password confirmation <span
                                            class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend"></div>
                                        <input type="password" name="password_confirmation"
                                               class="form-control m-input m-input--solid"
                                               placeholder="Confirm password"
                                                {{isset($user)? "" : "required"}}
                                        >
                                    </div>
                                    <span class="m-form__help">Please confirm your password</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Role <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <div class="m-radio-inline">
                                        @foreach($roles as $role)
                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="role"
                                                       @if(isset($user))
                                                       @if(old("role"))
                                                       {{$role->name === old("role")? "checked" : ""}}
                                                       @else
                                                       {{$user->hasRole($role->name)? "checked" : ""}}
                                                       @endif
                                                       @else
                                                       @if(old("role"))
                                                       {{$role->name === old("role")? "checked" : ""}}
                                                       @else
                                                       {{$role->name === \App\Models\Role::COMMON? "checked" : ""}}
                                                       @endif
                                                       @endif
                                                       value="{{$role->name}}"> {{$role->display_name}}
                                                <span></span>
                                            </label>
                                        @endforeach
                                    </div>
                                    <span class="m-form__help">Please select user role</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Avatar</label>
                                <div class="col-lg-6">
                                    <input type="file"
                                           class="filepond"
                                           name="avatar"
                                           accept="image/png, image/jpeg, image/gif"
                                           data-max-file-size="3MB"
                                           @if(isset($user) && !empty($user->avatar))
                                           data-exist-file="{{$user->avatar}}"
                                            @endif
                                    />
                                    @if ($errors->has('avatar'))
                                        <div id="avatar-error" class="form-control-feedback">
                                            {{ $errors->first('avatar') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{url('users')}}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
@endsection

@section('footerScript')
    <script src="{{url('/assets/demo/default/custom/crud/forms/widgets/input-mask.js')}}"
            type="text/javascript">
    </script>
@endsection