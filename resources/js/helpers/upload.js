class FileUpload {
    static readURL(inputSelector, previewSelector) {
        let input = document.querySelector(inputSelector);

        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                document.querySelector(previewSelector).setAttribute('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
}

export default FileUpload;