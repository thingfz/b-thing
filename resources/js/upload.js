import * as FilePond from 'filepond';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview'
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';

jQuery(document).ready(function () {
    FilePond.registerPlugin(
        FilePondPluginImagePreview,
        FilePondPluginImageExifOrientation,
        FilePondPluginFileValidateSize,
        FilePondPluginFileEncode,
        FilePondPluginFileValidateType
    );
    document.querySelectorAll('.filepond').forEach(item => {
        let options = {
            server: {
                process: null,
                load: (source, load, error, progress, abort, headers) => {
                    console.log('attempting to load', source);
                    fetch(source)
                        .then(res => res.blob())
                        .then(load)
                        .catch(error)
                }
            },
            instantUpload: false
        };
        if (item.dataset.existFile) {
            let fileOptions = [];
            if (item.hasAttribute('multiple')) {
                _.forEach(JSON.parse(item.dataset.existFile), file => {
                    fileOptions.push({
                        source: file,
                        options: {
                            type: 'local',
                            size: 3000000
                        }
                    })
                })
            } else {
                fileOptions.push({
                    source: item.dataset.existFile,
                    options: {
                        type: 'local',
                        size: 3000000
                    }
                })
            }
            options['files'] = fileOptions;
        }
        const pond = FilePond.create(
            item,
            options
        );

        pond.on('addfilestart', (error, file) => {
            $("button[type=submit]").attr("disabled", "disabled");
            if (error) {
                console.log('Oh no');
                return;
            }
            console.log('File added', file);
        });

        pond.on('addfile', (error, file) => {
            $("button[type=submit]").removeAttr("disabled");
            if (error) {
                console.log('Oh no');
                return;
            }
            console.log('File added', file);
        });
    });
});
