import React from 'react';
import {render} from 'react-dom';
import DataTableComponent from '../../../components/DataTableComponent';

let defColumns = [{
    "field": "name",
    "title": "Name",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": true,
    "template": function (row) {
        let name = row.name;
        let i = row.depth - 1;
        while (i) {
            name = '-' + name;
            i--;
        }
        return name;
    }
}, {
    "field": "created_at",
    "title": "Created At",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": false
}, {
    "field": "updated_at",
    "title": "Updated At",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": false
}];

function onInit(t) {
}

render(
    <DataTableComponent
        restful={true}
        resource='jobcategories'
        onInit={onInit}
        defColumns={defColumns}
    />,
    document.getElementById("jobCategoryTable"),
);
