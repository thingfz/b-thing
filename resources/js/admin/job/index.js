import React from 'react';
import {render} from 'react-dom';
import DataTableComponent from '../../components/DataTableComponent';

let defColumns = [{
    "field": "title",
    "title": "Title",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": true
}, {
    "field": "phone",
    "title": "Phone",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": true
}, {
    "field": "salary",
    "title": "Salary",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": false,
    template: '$ {{salary}}'
}, {
    "field": "created_at",
    "title": "Created At",
    "sortable": true,
    "selector": false,
    "textAlign": "center",
    "searchable": false
}, {
    "field": "updated_at",
    "title": "Updated At",
    "sortable": true,
    "selector": false,
    "textAlign": "center",
    "searchable": false
}];

function onInit(t) {
}

render(
    <DataTableComponent
        restful={true}
        resource='jobs'
        onInit={onInit}
        defColumns={defColumns}
    />,
    document.getElementById("jobTable"),
);
