import React from 'react';
import {render} from 'react-dom';
import DataTableComponent from '../../components/DataTableComponent';
let defColumns = [{
    "field": "name",
    "title": "Name",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": true
}, {
    "field": "email",
    "title": "Email",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": true
}, {
    "field": "roles",
    "title": "roles",
    "sortable": false,
    "selector": false,
    "textAlign": "center",
    "searchable": false,
    "template": function (row) {
        return row.roles.reduce((string, role) => {
            if (string === '') {
                return role.name
            }

            return `${string}, ${role.name}`
        }, '');
    }
}, {
    "field": "created_at",
    "title": "Created At",
    "sortable": true,
    "selector": false,
    "textAlign": "center",
    "searchable": false
}, {
    "field": "updated_at",
    "title": "Updated At",
    "sortable": true,
    "selector": false,
    "textAlign": "center",
    "searchable": false
}];

function onInit(t) {
    let formStatusSelector = $('#m_form_roles');

    formStatusSelector.on('change', function () {
        t.search($(this).val(), 'roles.name');
    });

    formStatusSelector.selectpicker();
}

render(
    <DataTableComponent
        restful={true}
        resource='users'
        onInit={onInit}
        defColumns={defColumns}
    />,
    document.getElementById("userTable"),
);
