<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'email' => 'Email',
    'name' => 'Full Name',
    'password' => 'Password',
    'login' => 'Login',
    'signUp' => 'Sign Up',
    'signIn' => 'Sign In',
    'rememberMe' => 'Remember me',
    'forgetPassword' => 'Forget Password ?',
    'withoutAccount' => 'Don\'t have an account yet ?',
    'signInToAdmin' => 'Sign In To Admin',
    'signUpToAdmin' => 'Enter your details to create your account:',

];
