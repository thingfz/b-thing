<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

$superAdmin = \App\Models\Role::SUPER_ADMIN;
$admin = \App\Models\Role::ADMIN;
//Admin
Route::namespace("Admin")->middleware(["auth", "role:{$superAdmin}|{$admin}"])->group(function () {
    Route::get('/', 'HomeController@index')->name('home')->middleware(["auth"]);
    Route::get('/home', 'HomeController@index')->name('home')->middleware(["auth"]);
    Route::resource('/users', 'UserController');
//Job Modules
    Route::resource('/jobs', 'JobController');
//Category Modules
    Route::resource('/jobcategories', 'JobCategoryController');
});

//End Admin