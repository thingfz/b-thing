let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/js/app.js', 'public/js')
    .react('resources/js/upload.js', 'public/js')
    .react('resources/js/admin/user/index.js', 'public/admin/user/index.js')
    .react('resources/js/admin/job/index.js', 'public/admin/job/index.js')
    .react('resources/js/admin/job/category/index.js', 'public/admin/job/category/index.js')
    .sass('resources/sass/app.scss', 'public/css');

mix.scripts([
    "public/js/upload.js",
    "public/js/app.js",
], "public/js/_all.js");