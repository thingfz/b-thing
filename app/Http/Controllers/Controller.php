<?php

namespace App\Http\Controllers;

use App\Contracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var BaseRepository
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param $modelClass
     */
    public function __construct($modelClass = Model::class)
    {
        $this->model = app()->makeWith(BaseRepository::class, ['modelClass' => $modelClass]);
    }
}
