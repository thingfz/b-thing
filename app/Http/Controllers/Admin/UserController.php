<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use App\Utility\DataTableUtility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Inject model to repository service
     *
     * User Controller constructor.
     */
    public function __construct()
    {
        parent::__construct(User::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->expectsJson()) {
            $scopes = [["scope" => "role", "parameters" => ["user_id" => Auth::id()]]];
            $pageInfo = DataTableUtility::getPageInfo($request);
            $searchColumns = DataTableUtility::getSearchColumns($request);
            $sortColumn = DataTableUtility::getSortColumn($request);
            $filterColumns = DataTableUtility::getFilterColumns($request);
            $result = $this->model->fetch(
                $pageInfo,
                $filterColumns,
                $sortColumn,
                $searchColumns,
                ['roles'],
                $scopes
            );
            $models = DataTableUtility::formatPageObject($result);
            return response()->json($models);
        }
        $roles = Role::all();
        return view('admin.user.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.user.createorupdate', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateStore($request);
        $uuid = Str::orderedUuid();
        $data = [
            "email" => $request->get('email'),
            "uuid" => $uuid,
            "name" => $request->get('name'),
            // Extract number from phone string format
            "phone" => preg_replace('/\D/', '', $request->get('phone')),
            "password" => Hash::make($request->get('password'))
        ];
        if ($request->has('avatar')) {
            $path = "users/$uuid/avatar.png";
            $file = base64_decode(json_decode($request->get('avatar'))->data);
            $isUpload = Storage::cloud()->put($path, $file, 'public');
            if ($isUpload) {
                $data['avatar'] = Storage::disk('s3')->url($path);
            }
        }
        $user = $this->model->create($data);
        $role = Role::where("name", $request->get('role'))->first();
        $user->attachRole($role);
        return redirect()->to("users");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $user = $this->model->findOneOrFail($id);
        return view('admin.user.createorupdate', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateUpdate($request, $id);
        $data = [
            "email" => $request->get('email'),
            "name" => $request->get('name'),
            // Extract number from phone string format
            "phone" => preg_replace('/\D/', '', $request->get('phone')),
            "avatar" => null
        ];
        if ($request->get('password')) {
            $data['password'] = Hash::make($request->get('password'));
        }

        $user = $this->model->findOneOrFail($id);
        if ($request->has('avatar')) {
            $path = "users/$user->uuid/avatar.png";
            $file = base64_decode(json_decode($request->get('avatar'))->data);
            $isUpload = Storage::cloud()->put($path, $file, 'public');
            if ($isUpload) {
                $data['avatar'] = Storage::disk('s3')->url($path);
            }
        }
        $this->model->update($data, $id);
        $user->detachRoles();
        $role = Role::where("name", $request->get('role'))->first();
        $user->attachRole($role);
        return redirect()->to("users");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(User::destroy($id));
    }

    /**
     * Validate User Store
     *
     * @param Request $request
     */
    protected function validateStore(Request $request)
    {
        $data = $request->all();
        $data['phone'] = preg_replace('/\D/', '', $request->get('phone'));
        Validator::make($data, [
            'name' => 'required|string|max:60',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            "role" => "required",
            'phone' => Rule::unique('users')
        ])->validate();
    }

    /**
     * Validate User Update
     *
     * @param Request $request
     * @param $id
     */
    protected function validateUpdate(Request $request, $id)
    {
        $data = $request->all();
        $data['phone'] = preg_replace('/\D/', '', $request->get('phone'));
        Validator::make($data, [
            "email" => [
                "required",
                "email",
                "max:255",
                Rule::unique("users", "email")->ignore($id),
            ],
            'password' => 'nullable|string|min:6|confirmed',
            "name" => "required|max:60",
            "role" => "required",
            'phone' => Rule::unique('users')->ignore($id)
        ])->validate();
    }
}
