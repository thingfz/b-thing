<?php

namespace App\Http\Controllers\Admin;

use App\Models\Image;
use App\Models\Job;
use App\Utility\DataTableUtility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class JobController extends Controller
{
    /**
     * Inject model to repository service
     *
     * User Controller constructor.
     */
    public function __construct()
    {
        parent::__construct(Job::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->expectsJson()) {
            $pageInfo = DataTableUtility::getPageInfo($request);
            $searchColumns = DataTableUtility::getSearchColumns($request);
            $sortColumn = DataTableUtility::getSortColumn($request);
            $filterColumns = DataTableUtility::getFilterColumns($request);
            $result = $this->model->fetch(
                $pageInfo,
                $filterColumns,
                $sortColumn,
                $searchColumns
            );
            $models = DataTableUtility::formatPageObject($result);
            return response()->json($models);
        }
        return view('admin.job.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.job.createorupdate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateStore($request);
        $uuid = Str::orderedUuid();
        $data = [
            "uuid" => $uuid,
            "title" => $request->get('title'),
            "phone" => preg_replace('/\D/', '', $request->get('phone')),
            "description" => $request->get('description'),
            "status" => intval($request->get('status')),
            "is_feature" => intval($request->has('is_feature')),
            "is_negotiable" => intval($request->has('is_negotiable')),
            "sort_order" => intval($request->get('sort_order')),
            "salary" => intval($request->get('salary')),
            "user_id" => Auth::id()
        ];
        $job = $this->model->create($data);
        if ($request->has('images') && is_array($request->get('images'))) {
            foreach ($request->get('images') as $key => $image) {
                $path = "jobs/$uuid/$key.png";
                $file = base64_decode(json_decode($image)->data);
                $isUpload = Storage::disk('s3')->put($path, $file, 'public');
                if ($isUpload) {
                    $url = Storage::disk('s3')->url($path);
                    $job->images()->save(new Image(['url' => $url]));
                }
            }
        }
        return redirect()->to("jobs");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = $this->model->findOneOrFail($id);
        return view('admin.job.createorupdate', compact('job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateUpdate($request);
        $data = [
            "title" => $request->get('title'),
            "phone" => preg_replace('/\D/', '', $request->get('phone')),
            "description" => $request->get('description'),
            "status" => intval($request->get('status')),
            "is_feature" => intval($request->has('is_feature')),
            "is_negotiable" => intval($request->has('is_negotiable')),
            "sort_order" => intval($request->get('sort_order')),
            "salary" => intval($request->get('salary')),
        ];

        $job = $this->model->findOneOrFail($id);

        if ($request->has('images') && is_array($request->get('images'))) {
            foreach ($request->get('images') as $key => $image) {
                $path = "jobs/$job->uuid/$key.png";
                $file = base64_decode(json_decode($image)->data);
                $isUpload = Storage::disk('s3')->put($path, $file, 'public');
                if ($isUpload) {
                    $url = Storage::disk('s3')->url($path);
                    $job->images()->save(new Image(['url' => $url]));
                }
            }
        } else {
            $job->images()->delete();
        }

        $this->model->update($data, $id);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(Job::destroy($id));
    }

    /**
     * Validate User Store
     *
     * @param Request $request
     */
    protected function validateStore(Request $request)
    {
        Validator::make($request->all(), [
            'title' => 'required|string|max:60',
            'phone' => 'required',
        ])->validate();
    }

    /**
     * Validate User Update
     *
     * @param Request $request
     */
    protected function validateUpdate(Request $request)
    {
        Validator::make($request->all(), [
            'title' => 'required|string|max:60',
            'phone' => 'required',
        ])->validate();
    }
}
