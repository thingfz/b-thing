<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\JobCategory;
use App\Utility\DataTableUtility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobCategoryController extends Controller
{
    /**
     * Inject model to repository service
     *
     * User Controller constructor.
     */
    public function __construct()
    {
        parent::__construct(JobCategory::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (request()->expectsJson()) {
            $pageInfo = DataTableUtility::getPageInfo($request);
            $searchColumns = DataTableUtility::getSearchColumns($request);
            $sortColumn = ['lft' => 'asc'];
            $filterColumns = DataTableUtility::getFilterColumns($request);
            $result = $this->model->fetch(
                $pageInfo,
                $filterColumns,
                $sortColumn,
                $searchColumns
            );
            $models = DataTableUtility::formatPageObject($result);
            return response()->json($models);
        }
        return view('admin.job.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobCategories = Category::where('name', '工作招聘')->first()->getDescendantsAndSelf()->toHierarchy();
        return view('admin.job.category.createorupdate', compact('jobCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobCategories = Category::where('name', '工作招聘')->first()->getDescendantsAndSelf()->toHierarchy();
        $jobCategory = Category::findOrFail($id);
        return view('admin.job.category.createorupdate', compact('jobCategories', 'jobCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(JobCategory::findOrFail($id)->delete());
    }
}
