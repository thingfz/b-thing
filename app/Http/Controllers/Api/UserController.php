<?php

namespace App\Http\Controllers\Api;

use App\Contracts\UserRepository;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UserController extends ApiController
{
    protected $service;

    /**
     * Inject model to repository service
     *
     * User Controller constructor.
     * @param UserRepository $service
     */
    public function __construct(UserRepository $service)
    {
        parent::__construct(User::class);
        $this->service = $service;
    }

    /**
     * Retrieve user by email and password.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrieveUserByCredential(Request $request)
    {
        $this->validateRetrieveUserByCredential($request);
        if ($request->expectsJson()) {
            $password = $request->get("password");
            $user = null;
            if ($request->has("email")) {
                $email = $request->get("email");
                $user = $this->service->retrieveByEmailAndPassword($email, $password);
            } else if ($request->has("phone_number")) {
                $phoneNumber = $request->get("phone_number");
                $user = $this->service->retrieveByPhoneNumberAndPassword($phoneNumber, $password);
            }
            return response()->json($user);
        } else {
            return null;
        }
    }

    /**
     * Validate retrieve user.
     *
     * @param Request $request
     */
    public function validateRetrieveUserByCredential(Request $request)
    {
        Validator::make($request->all(), [
            "email" => "required_without:phone_number",
            "phone_number" => [
                "required_without:email",
                'regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            ],
            "password" => "required|max:255|min:6",
        ])->validate();
    }
}
