<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function index(Request $request)
    {
        if (request()->expectsJson()) {
            $filterColumns = $request->get('filter_columns', []);
            if (!is_array($filterColumns)) {
                $filterColumns = json_decode($request->get('filter_columns'), true);
            }
            $searchColumns = $request->get('search_columns', []);
            if (!is_array($searchColumns)) {
                $searchColumns = json_decode($request->get('search_columns'), true);
            }
            $orderColumns = $request->get('order_columns', []);
            if (!is_array($orderColumns)) {
                $orderColumns = json_decode($request->get('order_columns', []), true);
            }
            $eagerLoading = $request->get('eager_loading', []);
            if (!is_array($eagerLoading)) {
                $eagerLoading = json_decode($request->get('eager_loading'), true);
            }
            $selectColumns = $request->get('select_columns', ['*']);
            if (!is_array($selectColumns)) {
                $selectColumns = json_decode($request->get('select_columns'), true);
            }

            $scopes = [];
            if (is_array($request->get('scopes'))) {
                $scopesRequest = $request->get('scopes');
                foreach ($scopesRequest as $scopeName => $parameters) {
                    $scopes[] = ['scope' => $scopeName, 'parameters' => $parameters];
                }
            }

            $pageSize = request()->get('page_size', $this->model::PAGE_SIZE);
            $page = request()->get('page', 1);
            $pageInfo = ['pageSize' => $pageSize, 'page' => $page];
            $pageData = $this->model->fetch($pageInfo, $filterColumns, $orderColumns, $searchColumns, $eagerLoading, $scopes, $selectColumns);
            return response()->json($pageData);
        } else {
            return null;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse|null
     * @throws \Exception
     */
    public function show($id)
    {
        if (request()->expectsJson()) {
            $eagerLoading = request()->get('eager_loading', []);
            $result = $this->model->with($eagerLoading)->find($id);
            if (!isset($result)) {
                abort(404, 'No records found.');
            }
            return response()->json($result);
        } else {
            return null;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function destroy($id)
    {
        if (request()->expectsJson()) {
            return response()->json($this->model->delete($id));
        } else {
            return null;
        }
    }
}
