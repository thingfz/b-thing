<?php

namespace App\Providers;

use App\Contracts\BaseRepository;
use App\Services\BaseRepositoryImpl;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use phpDocumentor\Reflection\Types\Null_;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        URL::forceScheme('https');
        error_reporting(E_ALL ^ E_DEPRECATED);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseRepository::class, function ($app, $parameters) {
            $modelClass = $parameters['modelClass'];
            if ($modelClass === Model::class) {
                return null;
            }
            return new BaseRepositoryImpl(new $modelClass());
        });
    }
}
