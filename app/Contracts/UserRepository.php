<?php

namespace App\Contracts;


interface UserRepository
{
    /**
     * @param string $email
     * @param string $password
     * @return mixed
     */
    public function retrieveByEmailAndPassword(string $email, string $password);

    /**
     * @param int $phoneNumber
     * @param string $password
     * @return mixed
     */
    public function retrieveByPhoneNumberAndPassword(int $phoneNumber, string $password);
}