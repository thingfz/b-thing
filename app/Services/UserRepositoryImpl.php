<?php

namespace App\Services;


use App\Contracts\BaseRepository;
use App\Contracts\UserRepository;
use App\Exceptions\ValidationException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserRepositoryImpl implements UserRepository
{
    /**
     * @var BaseRepository
     */
    protected $model;

    /**
     * Inject model to repository service
     *
     * User Controller constructor.
     */
    public function __construct()
    {
        $this->model = app()->makeWith(BaseRepository::class, ['modelClass' => User::class]);
    }

    /**
     * @param string $email
     * @param string $password
     * @return mixed
     * @throws ValidationException
     */
    public function retrieveByEmailAndPassword(string $email, string $password)
    {
        Log::debug(get_class($this) . "::retrieveUserByCredential => Retrieve a user by credential(email and password).");
        $user = $this->model->findOneByOrFail(["email" => $email]);
        if (Hash::check($password, $user->password)) {
            return $user;
        } else {
            throw new ValidationException('Credential is not match.', 401);
        }
    }

    /**
     * @param int $phoneNumber
     * @param string $password
     * @return mixed
     */
    public function retrieveByPhoneNumberAndPassword(int $phoneNumber, string $password)
    {
        // TODO: Implement retrieveByPhoneNumberAndPassword() method.
    }
}