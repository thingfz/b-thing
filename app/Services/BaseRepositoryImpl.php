<?php

namespace App\Services;


use App\Contracts\BaseRepository;
use App\Models\User;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

class BaseRepositoryImpl implements BaseRepository
{
    protected $model;
    protected $with = array();

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * @param array $attributes
     * @param int $id
     * @return bool
     */
    public function update(array $attributes, int $id): bool
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc')
    {
        return $this->newQuery()->orderBy($orderBy, $sortBy)->get($columns);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->newQuery()->find($id);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneOrFail(int $id)
    {
        return $this->newQuery()->findOrFail($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function findBy(array $data)
    {
        return $this->newQuery()->where($data)->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function findOneBy(array $data)
    {
        return $this->newQuery()->where($data)->first();
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneByOrFail(array $data)
    {
        return $this->newQuery()->where($data)->firstOrFail();
    }

    /**
     * @param array $data
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginateArrayResults(array $data, int $perPage = 20)
    {
        $page = request()->get('page', 1);
        $offset = ($page * $perPage) - $perPage;
        return new LengthAwarePaginator(
            array_slice($data, $offset, $perPage, false),
            count($data),
            $perPage,
            $page,
            [
                'path' => request()->url(),
                'query' => request()->query()
            ]
        );
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->find($id)->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function newQuery()
    {
        return $this->model->newQuery()->with($this->with);
    }

    /**
     * returns the repository itself, for fluent interface
     *
     * @param array $with
     * @return self
     */
    public function with(array $with)
    {
        $this->with = array_merge($this->with, $with);
        return $this;
    }

    /**
     * Fetch page object by table's name , page size, searching info ,and ordering info;
     *
     * $modelClass : The  Class Name of eloquent model.
     * Page Info : page num and page size.
     * Filter Columns : Key : column's name, Value : filter value.
     * Search Columns :  Key : column's name, Value : search value
     * Order Columns : Key : column's name, Value : ordering type ("asc", or "desc")
     * Eager Loading : Eager Loading attributes;
     *
     * @param array $pageInfo
     * @param array $filterColumn
     * @param array $orderColumn
     * @param array $searchColumn
     * @param array $eagerLoading
     * @param array $scopes
     * @param array $selectColumn
     * @return mixed
     */
    public function fetch(array $pageInfo = [], array $filterColumn = [], array $orderColumn = [], array $searchColumn = [], array $eagerLoading = [], array $scopes = [], array $selectColumn = ['*'])
    {
        Log::debug(get_class($this) . '::fetch => Fetch page object by table\'s name , page size, searching info ,and ordering info.');

        $query = $this->model->newQuery();

        if (isset($scopes) && sizeof($scopes) > 0) {
            foreach ($scopes as $scope) {
                if (isset($scope['parameters']))
                    $query->{$scope['scope']}($scope['parameters']);
                else
                    $query->{$scope['scope']}();
            }
        }

        if (isset($filterColumn) && sizeof($filterColumn) > 0) {
            $query->where(function ($q) use ($filterColumn) {
                foreach ($filterColumn as $column => $filter) {
                    if (strpos($column, '.') !== false) {
                        $relationColumn = explode('.', $column);
                        $className = Container::getInstance()->getNamespace() . 'Models\\' . ucfirst($relationColumn[0]);
                        if (class_exists($className)) {
                            $relationTable = (new $className)->getTable();
                        } else {
                            $relationTable = $relationColumn[0];
                        }
                        if (isset($filter)) {
                            if (is_array($filter) && array_get($filter, 'type') == false) {
                                $q->whereDoesntHave($relationColumn[0], function ($relateQuery) use ($relationTable, $relationColumn, $filter) {
                                    $this->generateCriteria($relateQuery, $relationTable . '.' . $relationColumn[1], $filter);
                                });
                            } else {
                                $q->whereHas($relationColumn[0], function ($relateQuery) use ($relationTable, $relationColumn, $filter) {
                                    $this->generateCriteria($relateQuery, $relationTable . '.' . $relationColumn[1], $filter);
                                });
                            }
                        } else {
                            $q->has($relationColumn[0]);
                        }
                    } else {
                        $q = $this->generateCriteria($q, $column, $filter);
                    }
                }
            });
        }

        if (isset($searchColumn) && sizeof($searchColumn) > 0) {
            $query->where(function ($q) use ($searchColumn) {
                foreach ($searchColumn as $column => $search) {
                    if (strpos($column, '.') !== false) {
                        $relationColumn = explode('.', $column);
                        $className = Container::getInstance()->getNamespace() . 'Models\\' . ucfirst($relationColumn[0]);
                        $relationTable = (new $className)->getTable();
                        $q->orWhereHas($relationColumn[0], function ($relateQuery) use ($relationTable, $relationColumn, $search) {
                            $relateQuery->where($relationTable . '.' . $relationColumn[1], 'like', '%' . $search . '%');
                        });
                    } else {
                        $q->orWhere($column, 'like', '%' . $search . '%');
                    }
                }
            });
        }

        if (isset($eagerLoading) && sizeof($eagerLoading) > 0) {
            foreach ($eagerLoading as $value) {
                $query = $query->with($value);
            }
        }

        if (isset($orderColumn) && sizeof($orderColumn) > 0) {
            foreach ($orderColumn as $column => $dir) {
                if (strpos($column, '.') !== false) {
                    $relationColumn = explode('.', $column);
                    $query->with([$relationColumn[0] => function ($relateQuery) use ($relationColumn, $dir) {
                        $relateQuery->orderBy($relationColumn[1], $dir);
                    }]);

                } else {
                    $query->orderBy($column, $dir);
                }
            }
        } else {
            $query->orderBy('updated_at', 'desc');
        }

        if (isset($pageInfo) && array_get($pageInfo, 'pageSize')) { // if the page info exists , then fetch the pagination info.
            $perPage = $pageInfo['pageSize'] ?: self::PAGE_SIZE;
            $page = $pageInfo['page'] ?: 1;
            $result = $query->select($selectColumn)->paginate($perPage, null, null, $page);
        } else {
            $result = $query->get($selectColumn);
        }

        return $result;
    }

    /**
     * @param $query
     * @param $column
     * @param $filter
     *
     * @return object
     */
    protected function generateCriteria(Builder $query, $column, $filter)
    {
        if (is_array($filter)) {
            $operation = array_get($filter, 'operation');
            $value = array_get($filter, 'value');
            if ('isNull' == $operation) {
                $query->whereNull($column);
            } else if ('isNotNull' == $operation) {
                $query->whereNotNull($column);
            } else if ('in' == $operation && is_array($value)) {
                $query->whereIn($column, $value);
            } else if ('notIn' == $operation && is_array($value)) {
                $query->whereNotIn($column, $value);
            } else if ('between' == $operation && is_array($value)) {
                $query->whereBetween($column, $value);
            } else {
                $query->where($column, $operation, $value);
            }
        } else {
            if (!isset($filter)) {
                $query->whereNull($column);
            } else {
                $query->where($column, '=', $filter);
            }
        }

        return $query;
    }
}