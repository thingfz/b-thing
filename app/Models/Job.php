<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    const STATUS_APPROVED = 2;
    const STATUS_PENDING = 1;
    const STATUS_REJECTED = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "jobs";

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get all of the job's images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    /**
     * Get the user of the job.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
