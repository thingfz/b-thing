<?php

namespace App\Models;

use App\Scopes\JobCategoryScope;

class JobCategory extends Category
{
    const ROOT_NAME = '工作招聘';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $orderColumn = 'lft';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new JobCategoryScope);
    }

    public function getRoot()
    {
        return $this->where('name', '工作招聘')->first();
    }
}
