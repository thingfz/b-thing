<?php

namespace App\Models;


class Category extends \Baum\Node
{
    const TYPE_JOB = 'job';
    const TYPE_UNIT = 'unit';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';
}
