<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'uuid', 'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A user has one or more roles.
     */
    public function roles()
    {
        return $this->belongsToMany("App\\Models\\Role", "role_user", "user_id", "role_id");
    }

    /**
     * Scope: select the user list by user'role.
     *
     * @param $query
     * @param $options
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRole($query, $options)
    {
        if (!empty($options['user_id'])) {
            $user = User::find($options['user_id']);
            if ($user->hasRole(Role::SUPER_ADMIN)) {
                return $query;
            } else if ($user->hasRole(Role::ADMIN)) {
                $query->whereDoesntHave('roles', function ($q) {
                    $q->where('name', '=', Role::SUPER_ADMIN);
                });
            }
        }
        return $query;
    }
}
