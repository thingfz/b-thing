<?php

namespace App\Scopes;

use App\Models\Category;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class JobCategoryScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $jobCategory = Category::where('name', '工作招聘')->first();
        $builder->whereBetween('lft', [$jobCategory->lft, $jobCategory->rgt])
            ->where('id', '!=', $jobCategory->id);
    }
}