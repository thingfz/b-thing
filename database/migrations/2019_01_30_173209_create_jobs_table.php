<?php

use App\Models\Job;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->string('title');
            $table->string('phone');
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default(Job::STATUS_APPROVED)->comment('0: unavailable,  1: available');
            $table->tinyInteger('feature_type')->default(1)->comment('Push top rule');
            $table->boolean('is_feature')->default(0);
            $table->boolean('is_negotiable')->default(0);
            $table->integer("sort_order")->default(0);
            $table->integer("salary")->default(0);
            $table->integer("clicks")->default(0);
            $table->timestamps();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
