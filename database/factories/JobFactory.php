<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Job::class, function (Faker $faker) {
    return [
        'uuid' => Str::orderedUuid(),
        'title' => $faker->title,
        'phone' => $faker->phoneNumber,
        'description' => $faker->paragraph,
        'status' => \App\Models\Job::STATUS_APPROVED,
        'feature_type' => '1',
        'is_feature' => '0',
        'is_negotiable' => '0',
        'sort_order' => '0',
        'salary' => '3000',
        'user_id' => 1,
    ];
});
