<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job = Category::create(['name' => '工作招聘']);
        $restaurantJobCategory = $job->children()->create(['name' => '餐馆招聘']);
        $jJob = $restaurantJobCategory->children()->create(['name' => '日餐']);
        $jJob->children()->create(['name' => 'Sushi']);
        $restaurantJobCategory->children()->create(['name' => '中餐']);
        $restaurantJobCategory->children()->create(['name' => '自助餐']);
        $restaurantJobCategory->children()->create(['name' => '夫妻工']);
        $restaurantJobCategory->children()->create(['name' => '其他餐饮']);
        $nailCategory = $job->children()->create(['name' => '美甲招聘']);
        $nailCategory->children()->create(['name' => '大工']);
        $nailCategory->children()->create(['name' => '中工']);
        $nailCategory->children()->create(['name' => '小工']);
        $nailCategory->children()->create(['name' => '学徒/其他']);

        $unit = Category::create(['name' => '房屋租凭']);
        $unit->children()->create(['name' => '整房']);
        $unit->children()->create(['name' => '单房']);
        $unit->children()->create(['name' => '套房']);
        $unit->children()->create(['name' => '合租']);
        $unit->children()->create(['name' => '商铺']);
        $unit->children()->create(['name' => '仓库']);
    }
}
