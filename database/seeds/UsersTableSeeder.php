<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = new Role();
        $superAdmin->name = Role::SUPER_ADMIN;
        $superAdmin->display_name = 'Super Admin'; // optional
        $superAdmin->save();

        $admin = new Role();
        $admin->name = Role::ADMIN;
        $admin->display_name = 'Admin'; // optional
        $admin->save();

        $common = new Role();
        $common->name = Role::COMMON;
        $common->display_name = 'Common'; // optional
        $common->save();

        $invalid = new Role();
        $invalid->name = Role::INVALID;
        $invalid->display_name = 'Invalid'; // optional
        $invalid->save();

        $user = new User();
        $user->fill([
            'name' => 'ken',
            'uuid' => Str::orderedUuid(),
            'email' => 'luckykenlin@gmail.com',
            'phone' => 9172429085,
            'password' => bcrypt('Lkk168968'),
        ]);
        $user->save();
        $user->attachRole($superAdmin);

        $adminUser = new User();
        $adminUser->fill([
            'name' => 'ken',
            'uuid' => Str::orderedUuid(),
            'email' => 'luckykenlin@icloud.com',
            'password' => bcrypt('Lkk168968'),
        ]);
        $adminUser->save();
        $adminUser->attachRole($admin);

        factory(App\Models\User::class, 50)->create()->each(function ($user) use ($common) {
            $user->attachRole($common);
        });
    }
}
